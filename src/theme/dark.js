import { createTheme } from "@mui/material/styles";

const darkTheme = createTheme({
  typography: {
    fontFamily: ["roboto"],
  },
  palette: {
    mode: "dark",
    primary: {
      main: "#fff",
      contrast: "#000",
      contrastBg: "#000",
    },
    secondary: {
      main: "#ffff",
      contrast: "#308fe8",
    },
    background: {
      main: "#101418",
      secondary: "#0f1924",
    },
  },
});

export { darkTheme };
