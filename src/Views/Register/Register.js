import {
  Button,
  Container,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useAuth } from "../../context/Loggin";
import { useFormik } from "formik";
import * as yup from "yup";
import { useStyles } from "./RegisterStyles";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { images } from "../../asets/images";

export const Register = () => {
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);

  const { registerUser } = useAuth();

  const validationSchema = yup.object({
    password: yup
      .string("se requiere una contraseña con letras")
      .min(6, "se requiere una contraseña con almenos 6 caracteres")
      .matches(
        /^[a-zA-Z0-9_.]*$/,
        "La contraseña de usuario solo puede contener caracteres alfanuméricos, guiones bajos y puntos."
      )
      .required("La contraseña es requerida"),
    email: yup
      .string("Se requiere un correo con letras")
      .email("Se requiere un correo con un formato valido")
      .required("El correo es un campo obligatorio"),
  });
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema,
    onSubmit: (values) => {
      registerUser(values);
    },
  });
  return (
    <div className={classes.mainContainer}>
      <Container>
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Typography className={classes.title}>
                Registrar usuario
              </Typography>
            </Grid>
            <Grid item md={12} xs={12} className={classes.imagencontainer}>
              <img src={images.wortevLogo} alt="wortev-logo" />
            </Grid>
            <Grid item md={12} xs={12} className={classes.inputContainer}>
              <TextField
                fullWidth
                value={formik.values.email}
                onChange={formik.handleChange}
                label="Correo electrónico"
                placeholder="Correo electrónico"
                type="email"
                id="email"
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              <TextField
                type={showPassword ? "text" : "password"}
                label="Contraseña"
                placeholder="Contraseña"
                id="password"
                name="password"
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => setShowPassword(!showPassword)}
                        edge="end"
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                className="input"
                variant="outlined"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
              />
            </Grid>
            <Grid item md={12} xs={12} className={classes.containerButton}>
              <Button variant="contained" color="secondary" type="submit">
                Enviar
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </div>
  );
};
