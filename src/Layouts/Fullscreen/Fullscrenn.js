import React from "react";

export const FullscrenLayout = ({ children }) => {
  return <div>{children}</div>;
};
