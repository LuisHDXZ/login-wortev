import { createContext, useContext, useEffect, useState } from "react";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
  GoogleAuthProvider,
  signInWithPopup,
} from "firebase/auth";
import { auth } from "../../firebase";
import { useNavigate } from "react-router-dom";

const authContext = createContext();

export const useAuth = () => {
  const context = useContext(authContext);
  if (!context) throw new Error("There is no Auth provider");
  return context;
};

export function LogginState({ children }) {
  const [user, setUser] = useState(null);
  const navigate = useNavigate();

  const registerUser = ({ email, password }) => {
    createUserWithEmailAndPassword(auth, email, password)
      .then(() => navigate("/Home"))
      .catch((error) => console.error(error));
  };

  const handleLoggin = ({ email, password }) => {
    // return signInWithEmailAndPassword(auth, email, password);
    signInWithEmailAndPassword(auth, email, password)
      .then(() => {
        navigate("/Home");
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const loginWithGoogle = () => {
    const googleProvider = new GoogleAuthProvider();
    signInWithPopup(auth, googleProvider)
      .then(() => {
        navigate("/Home");
      })
      .catch((error) => console.error(error));
  };

  const logout = () => {
    signOut(auth)
      .then(() => {
        navigate("/");
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    const unsubuscribe = onAuthStateChanged(auth, (currentUser) => {
      setUser(currentUser);
    });
    return () => unsubuscribe();
  }, []);

  return (
    <authContext.Provider
      value={{
        registerUser,
        handleLoggin,
        user,
        logout,
        loginWithGoogle,
      }}
    >
      {children}
    </authContext.Provider>
  );
}
