import {
  Button,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useStyles } from "./LoginStyles";
import { useFormik } from "formik";
import * as yup from "yup";
import { useAuth } from "../../context/Loggin";
import { images } from "../../asets/images";
import GoogleIcon from "@mui/icons-material/Google";

export const Login = () => {
  const [showPassword, setShowPassword] = useState(false);
  const classes = useStyles();

  const { handleLoggin, loginWithGoogle } = useAuth();

  const validationSchema = yup.object({
    password: yup
      .string("se requiere una contraseña con letras")
      .min(6, "se requiere una contraseña con almenos 6 caracteres")
      .matches(
        /^[a-zA-Z0-9_.]*$/,
        "La contraseña de usuario solo puede contener caracteres alfanuméricos, guiones bajos y puntos."
      )
      .required("La contraseña es requerida"),
    email: yup
      .string("Se requiere un correo con letras")
      .email("Se requiere un correo con un formato valido")
      .required("El correo es un campo obligatorio"),
  });
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema,
    onSubmit: (values) => {
      handleLoggin(values);
    },
  });

  return (
    <Grid container className={classes.mainContainer}>
      <Grid item md={6} xs={12} className={classes.imageContainer}>
        <img
          src={images.wortevLogo}
          alt="iconWortev"
          style={{ width: "70%" }}
        />
      </Grid>

      <Grid item md={6}>
        <form
          className={classes.form}
          key="Login"
          onSubmit={formik.handleSubmit}
          autoComplete="off"
        >
          <Grid
            style={{
              padding: "5vh",
            }}
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item md={12} xs={12}>
              <Typography className={classes.title}>Bienvenido</Typography>
            </Grid>
            <Grid item md={12} xs={12} className={classes.inputContainer}>
              <TextField
                fullWidth
                value={formik.values.email}
                onChange={formik.handleChange}
                label="Correo electrónico"
                placeholder="Correo electrónico"
                type="email"
                id="email"
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              <TextField
                type={showPassword ? "text" : "password"}
                label="Contraseña"
                placeholder="Contraseña"
                id="password"
                name="password"
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => setShowPassword(!showPassword)}
                        edge="end"
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                className="input"
                variant="outlined"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
              />
            </Grid>
            <Grid item md={12} xs={12} className={classes.containerButton}>
              <Button type="submit" variant="contained" color="secondary">
                INICIAR SESIÓN
              </Button>
            </Grid>
            <Grid item md={12} xs={12} className={classes.containerButton}>
              <Button
                variant="contained"
                color="secondary"
                startIcon={<GoogleIcon />}
                onClick={() => loginWithGoogle()}
              >
                Continuar con Goole
              </Button>
            </Grid>
            <Grid item md={12} xs={12} className={classes.containerButton}>
              <Typography className={classes.forgotPasswordText}>
                Si no tienes una cuenta da click <a href="/register">aqui</a>
              </Typography>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Grid>
  );
};
