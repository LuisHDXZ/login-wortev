import "./App.css";
import { ThemeProvider } from "@mui/material/styles";
import { lightTheme, darkTheme } from "./theme";
import { LogginState } from "./context/Loggin/LogginState";
import { Routes, Route } from "react-router-dom";
import { routes } from "./routes/routes";

function App() {
  return (
    <ThemeProvider theme={lightTheme}>
      <LogginState>
        <Routes>
          {routes.map((itemRoute) => (
            <Route
              path={itemRoute.path}
              exact={itemRoute.exact}
              element={
                <itemRoute.layout>{itemRoute.component}</itemRoute.layout>
              }
            />
          ))}
        </Routes>
      </LogginState>
    </ThemeProvider>
  );
}

export default App;
