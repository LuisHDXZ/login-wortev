// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCHYSttEncs0-pg63P0SPXq-GMLBu_DBvg",
  authDomain: "aut-wortev.firebaseapp.com",
  projectId: "aut-wortev",
  storageBucket: "aut-wortev.appspot.com",
  messagingSenderId: "161090172394",
  appId: "1:161090172394:web:04c7a8a07ba2168928ee93",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export { app, auth };
