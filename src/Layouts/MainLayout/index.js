import React from "react";
import { Header } from "../../Components/Header/Header";
import { Grid } from "@mui/material";

export const MainLayout = ({ children }) => {
  return (
    <Grid container>
      <Grid item md={12} xs={12}>
        <Header />
      </Grid>
      <Grid item md={12} xs={12}>
        {children}
      </Grid>
    </Grid>
  );
};
