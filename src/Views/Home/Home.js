import React, { useEffect, useState } from "react";
import { Button, Grid, MenuItem, TextField, Typography } from "@mui/material";

export const Home = () => {
  const [holydays, setholydays] = useState([]);
  const [year, setYear] = useState([]);
  const [yearSelect, setyearSelect] = useState(null);
  const [monthSelect, setmonthSelect] = useState(null);
  const [hoursWorked, sethoursWorked] = useState(null);
  const [daySelect, setdaySelect] = useState(null);
  const getDaysInMonth = (year, month) => new Date(year, month, 0).getDate();
  const months = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];

  const countHours = () => {
    // inicializamos un contador de horas
    let totalHours = 0;
    // Obtenemos el día del año de cada día festivo
    holydays.map((holiday) => {
      //Se convierte el dia festivo  a fecha
      const newDate = new Date(`${holiday}/${year}`);
      // se obtiene el dia festivo
      const day = newDate.getDay();
      //Obtenemos el numero de dias en el mes
      const numberofDays = getDaysInMonth(year, newDate.getMonth());
      //se valida que el dia no sea mayor al numero de dias que tine en el mes
      if (numberofDays > day) {
        // chequeamos que no sea un domingo, día 0 en el que no se crearía hora extra
        if (day !== 0 && day !== 6) {
          //Sumamos 2horas extra al contador si no es domingo
          totalHours += 2;
        }
      }
    });
    //Devolvemos la cantidad total de horas
    sethoursWorked(totalHours);
  };
  // const year = 2022;
  // const holidays = ["01/06", "04/01", "12/25", "02/29"];

  // countHours(year, holidays);

  const addHolyday = () => {
    const dataToUpdate = `${monthSelect}/${daySelect}`;
    if (holydays.indexOf(dataToUpdate) === -1) {
      setholydays([...holydays, `${monthSelect}/${daySelect}`]);
    }
  };

  useEffect(() => {
    createYears();
  }, []);
  useEffect(() => {
    holydays.length !== 0 && countHours();
  }, [holydays]);

  const createDayMonth = () => {
    const days = getDaysInMonth(yearSelect, monthSelect);
    const daysOfMont = [];
    for (let index = 0; index < days; index++) {
      daysOfMont.push(index + 1);
    }
    return daysOfMont;
  };

  const createYears = () => {
    const actualYear = new Date().getFullYear();
    const separatedYear = actualYear.toString().split("");
    const year = `${separatedYear[separatedYear.length - 2]}${
      separatedYear[separatedYear.length - 1]
    }`;
    const arrayYears = [];
    for (let index = parseInt(year); index < parseInt(year) + 10; index++) {
      arrayYears.push(
        `${separatedYear[separatedYear.length - 4]}${
          separatedYear[separatedYear.length - 3]
        }${index}`
      );
    }
    setYear([...arrayYears]);
  };

  const restart = () => {
    setyearSelect(null);
    setmonthSelect(null);
    sethoursWorked(null);
    setdaySelect(null);
    setholydays([]);
    createYears();
  };

  return (
    <Grid container style={{ padding: "4vh" }}>
      <Grid item md={12} xs={12}>
        <Grid container spacing={2}>
          <Grid item md={3}>
            <TextField
              fullWidth
              select
              label="Año"
              disabled={yearSelect !== null}
              value={yearSelect}
              onChange={(e) => setyearSelect(parseInt(e.target.value))}
            >
              {year.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item md={3}>
            <TextField
              fullWidth
              select
              label="Mes"
              value={monthSelect}
              onChange={(e) => setmonthSelect(e.target.value)}
            >
              {months.map((option, index) => (
                <MenuItem key={option} value={index}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item md={3}>
            {monthSelect !== null && (
              <TextField
                fullWidth
                select
                label="dia"
                value={daySelect}
                onChange={(e) => setdaySelect(e.target.value)}
              >
                {createDayMonth().map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            )}
          </Grid>
          <Grid item md={3}>
            {daySelect !== null && (
              <>
                <Button onClick={addHolyday}>Agregar dia festivo</Button>
                <Button onClick={restart} disabled={holydays.length === 0}>
                  Reiniciar
                </Button>
              </>
            )}
          </Grid>
          <Grid item md={6}>
            <Typography>Dias seleccionados</Typography>
          </Grid>
          {(hoursWorked !== null) | (hoursWorked !== 0) && (
            <Grid item md={6}>
              <Typography>Usted trabajara {hoursWorked} horas</Typography>
            </Grid>
          )}
          {holydays !== null &&
            holydays.map((holyday) => (
              <Grid item md={12}>
                {holyday}
              </Grid>
            ))}
        </Grid>
      </Grid>
    </Grid>
  );
};
