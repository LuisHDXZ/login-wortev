import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: "100vh",
    backgroundColor: theme.palette.background.main,
  },
  title: {
    fontSize: "60px !important",
    textAlign: "center",
    color: `${theme.palette.primary.main} !important`,
  },

  forgotPassword: {
    display: "flex",
    textAlign: "center",
    width: "100%",
  },
  forgotPasswordText: {
    color: theme.palette.primary.main,
    fontSize: "14px !important",
    textAlign: "center !important",
    width: "100%",
    "& a": {
      color: theme.palette.primary.main,
    },
  },
  form: {
    minHeight: "100%",
    "& .MuiFormControl-root": {
      marginBottom: theme.spacing(2.5),
    },
  },
  imagencontainer: {
    display: "flex",
    maxHeight: "12vh",
    justifyContent: "center",
  },
  containerButton: {
    display: "flex",
    justifyContent: "center",
    padding: "2vh 1vh",
  },
  titleWelcome: {},
}));

export { useStyles };
