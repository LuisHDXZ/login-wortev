import { Home } from "../Views/Home";
import { Login } from "../Views/Login";
import { Register } from "../Views/Register";
import { PrivateRoutes } from "./privateRoutes";
import { MainLayout, FullscrenLayout } from "../Layouts";

const routes = [
  { path: "", exact: true, component: <Login />, layout: FullscrenLayout },
  {
    path: "/Home",
    exact: true,
    layout: MainLayout,
    component: (
      <PrivateRoutes>
        <Home />
      </PrivateRoutes>
    ),
  },
  {
    path: "/register",
    exact: true,
    component: <Register />,
    layout: FullscrenLayout,
  },
];

export { routes };
