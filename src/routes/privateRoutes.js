import { Navigate } from "react-router-dom";
import { useAuth } from "../context/Loggin";

const PrivateRoutes = ({ children }) => {
  const { user } = useAuth();
  if (!user) return <Navigate to="/" />;

  return <>{children}</>;
};

export { PrivateRoutes };
