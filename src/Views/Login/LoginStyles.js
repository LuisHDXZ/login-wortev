import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: "100vh",
    backgroundColor: theme.palette.background.main,
  },
  title: {
    fontSize: "60px !important",
    textAlign: "center",
    color: `${theme.palette.primary.main} !important`,
    paddingBottom: "2vh",
  },

  forgotPassword: {
    display: "flex",
    textAlign: "center",
    width: "100%",
  },
  forgotPasswordText: {
    color: theme.palette.primary.main,
    fontSize: "14px !important",
    textAlign: "center !important",
    width: "100%",
    "& a": {
      color: theme.palette.primary.main,
    },
  },
  form: {
    minHeight: "100%",
    "& .MuiFormControl-root": {
      marginBottom: theme.spacing(2.5),
    },
  },
  imageContainer: {
    display: "flex",
    minHeight: "100vh",
    backgroundColor: theme.palette.background.secondary,
    borderRadius: "0px 50px 50px 0px",
    justifyContent: "center",
    alignContent: "center",
    [theme.breakpoints.down("xs")]: {
      minHeight: "30vh",
    },
  },
  containerButton: {
    display: "flex",
    justifyContent: "center",
    padding: "2vh 1vh",
  },
  titleWelcome: {},
}));

export { useStyles };
